const express = require('express');
const app = express();
const port = process.env.PORT || 3001;


app.use(express.static('public'));


app.use(express.urlencoded({ extended: true }));


app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});


app.post('/submit', (req, res) => {
    const { name, email } = req.body;
    res.send(`Form submitted! Name: ${name}, Email: ${email}`);
});


app.listen(port, () => {
    console.log(`Server is listening at http://localhost:${port}`);

});
